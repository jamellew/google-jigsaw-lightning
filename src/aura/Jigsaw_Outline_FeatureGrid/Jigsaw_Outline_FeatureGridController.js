/**
 * Created by lee-anneclarke on 12/11/18.
 */
({
    goToSiteHome: function goToSiteHome(component, event) {
        var url = event.currentTarget.dataset.url;
        var action = $A.get('e.force:navigateToURL');
        action.setParams({
            'url': url
        });
        action.fire();
    },
})