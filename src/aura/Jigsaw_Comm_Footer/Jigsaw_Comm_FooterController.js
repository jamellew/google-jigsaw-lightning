/**
 * Created by lee-anneclarke on 12/9/18.
 */
({
    goToSite: function goToSite(component, event) {
        var url = event.currentTarget.dataset.url;
        var action = $A.get('e.force:navigateToURL');
        action.setParams({
            'url': url
        });
        action.fire();
    },
})