/**
 * Created by lee-anneclarke on 12/18/18.
 */
({
    doInit: function(component, event, helper) {
        /* Grab the upvote and downvote counts from Apex,
        and set their Lightning component values on load */

        var recordId = component.get("v.recordId");

        helper.doCallout(component, "c.loadArticleRating", {articleVersionId: recordId})
            .then($A.getCallback(function (result) {
                component.set("v.upvoteCount", result.Helpful_Count__c);
                component.set("v.downvoteCount", result.Not_Helpful_Count__c);
            }));
    },

    selectUpVote: function (component, event, helper) {
        var recordId = component.get("v.recordId");

        // Change the button styles
        component.set("v.upvoteIsSelected", true);

        /*
            Ensure that the votes are calculated correctly by checking to see
            if upvote was already chosen
            If so, decrease the upvote count by 1
            If not, just increase the downvote count
        */
        component.set("v.upvoteAlreadyChosen", true);
        var downvoteAlreadyChosen = component.get("v.downvoteAlreadyChosen");

        if (downvoteAlreadyChosen) {
            helper.doCallout(component, "c.rateArticle",
                {
                    articleVersionId: recordId,
                    helpful: true,
                    rollbackVote: true
                })
                .then($A.getCallback(function (result) {
                    console.log(result);
                }));
        } else {
            helper.doCallout(component, "c.rateArticle",
                {
                    articleVersionId: recordId,
                    helpful: true,
                    rollbackVote: ''
                })
                .then($A.getCallback(function (result) {
                    console.log(result);
                }));
        }

        // Reload the data
        helper.doCallout(component, "c.loadArticleRating", {articleVersionId: recordId})
            .then($A.getCallback(function (result) {
                component.set("v.upvoteCount", result.Helpful_Count__c);
                component.set("v.downvoteCount", result.Not_Helpful_Count__c);
            }));
    },

    selectDownVote: function (component, event, helper) {
        var recordId = component.get("v.recordId");

        // Change the button styles
        component.set("v.upvoteIsSelected", false);

        /*
            Ensure that the votes are calculated correctly by checking to see if upvote was already chosen
            If so, decrease the upvote count by 1
            If not, just increase the downvote count
        */
        component.set("v.downvoteAlreadyChosen", true);
        var upvoteAlreadyChosen = component.get("v.upvoteAlreadyChosen");

        if (upvoteAlreadyChosen) {
            helper.doCallout(component, "c.rateArticle",
                {
                    articleVersionId: recordId,
                    helpful: false,
                    rollbackVote: true
                })
                .then($A.getCallback(function (result) {
                    console.log(result);
                }));
        } else {
            helper.doCallout(component, "c.rateArticle",
                {
                    articleVersionId: recordId,
                    helpful: false,
                    rollbackVote: '',
                })
                .then($A.getCallback(function (result) {
                    console.log(result);
                }));
        }

        // Reload the data
        helper.doCallout(component, "c.loadArticleRating", {articleVersionId: recordId})
            .then($A.getCallback(function (result) {
                component.set("v.upvoteCount", result.Helpful_Count__c);
                component.set("v.downvoteCount", result.Not_Helpful_Count__c);
            }));
    },
});