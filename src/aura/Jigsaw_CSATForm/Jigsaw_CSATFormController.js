/**
 * Created by lee-anneclarke on 11/15/18.
 */
({
    doInit: function(component, event, helper) {

        // Grab the UID from the current url

        var uid = component.get("v.uid");
        var parsedUrl = new URL(window.location.href);
        uid = parsedUrl.searchParams.get("uid");

        // If there is record data, load it.
        // If not, show "Thank you for your submission" message.
        /*
            In the Apex code, the data will not load if it has already been filled out
            in order to prevent duplicate submissions.
        */

         helper.doCallout(component, "c.loadSurvey", {uniqueId: uid})
        .then($A.getCallback(function (result) {
            if(result) {
                component.set("v.recordId", result.Id);
                component.set("v.isInit", true);
            } else {
                var formMsgs = component.find("formMsgs");
                $A.util.removeClass(formMsgs, "slds-hide");
                component.set("v.formIsComplete", true);
            }
        }));
    },

    handleSubmit: function(component, event, helper) {
        event.preventDefault();
        var formMsgs = component.find("formMsgs");
        var eventFields = event.getParam("fields");

        /*
            Submit the form only if "Support Experience Rating" and "Is your issue resolved" have values.
            "Other Feedback" is optional.
            Show error message if required fields are not filled in.
        */

        if (
            eventFields["Support_Experience_Rating__c"] &&
            eventFields["Is_your_issue_resolved__c"]
        ) {
                // Ensure that the form can't be submitted twice by the same user
                eventFields["Completed__c"] = true;

                // Submit the form
                component.find("csatForm").submit(eventFields);

        } else {
            $A.util.removeClass(formMsgs, "slds-hide");
            component.set("v.formIsComplete", false);
            console.log("Please fill out all required fields.");
        }
    },

    handleSuccess: function(component) {
        // Hide the form & show the success message

        var csatForm = component.find("csatForm");
        var formMsgs = component.find("formMsgs");

        $A.util.addClass(csatForm, "slds-hide");
        $A.util.removeClass(formMsgs, "slds-hide");
        component.set("v.formIsComplete", true);
        console.log("Success!");
    },

    handleError: function(component) {
        console.log("Error!");
    }
});