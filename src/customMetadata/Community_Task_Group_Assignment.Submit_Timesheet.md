<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Submit Timesheet</label>
    <protected>false</protected>
    <values>
        <field>Group__c</field>
        <value xsi:type="xsd:string">Employees</value>
    </values>
    <values>
        <field>Task__c</field>
        <value xsi:type="xsd:string">Submit_Timesheet</value>
    </values>
</CustomMetadata>
