<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Brush</label>
    <protected>false</protected>
    <values>
        <field>Days_Until_Due__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Deactivate__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>Description__c</field>
        <value xsi:type="xsd:string">Brush</value>
    </values>
</CustomMetadata>
