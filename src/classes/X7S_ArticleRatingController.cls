/**
 * Created by jeffnelson on 12/11/18.
 */

public with sharing class X7S_ArticleRatingController {

    @AuraEnabled
    public static Article_Rating__c loadArticleRating (String articleVersionId) {
        Article_Rating__c retVal = null;
        for(Article_Rating__c ar : [
                SELECT
                        Helpful_Count__c
                        ,Not_Helpful_Count__c
                FROM
                        Article_Rating__c
                WHERE
                        Knowledge__c = :articleVersionId
                LIMIT
                        1]
        ) {
            retVal = ar;
        }

        if(null == retVal) {
            retVal = new Article_Rating__c(
                    Helpful_Count__c = 0
                    ,Not_Helpful_Count__c=0);
        }

        return retVal;
    }

    @AuraEnabled
    public static String rateArticle (
            String articleVersionId
            ,Boolean helpful
            ,Boolean rollbackVote) {

        Article_Rating__c rating = null;
        for(Article_Rating__c ar : [
                SELECT
                        Helpful_Count__c
                        ,Not_Helpful_Count__c
                FROM
                        Article_Rating__c
                WHERE
                        Knowledge__c = :articleVersionId
                LIMIT
                        1]
        ) {
            rating = ar;
        }

        if(null == rating) rating = new Article_Rating__c(
                Knowledge__c = articleVersionId
                ,Helpful_Count__c = 0
                ,Not_Helpful_Count__c = 0);

        if(helpful) {
            if(rollbackVote) {
                rating.Not_Helpful_Count__c--;
            }
            rating.Helpful_Count__c++;
        } else {
            if(rollbackVote) {
                rating.Helpful_Count__c--;
            }
            rating.Not_Helpful_Count__c++;
        }

        upsert rating;

        return OK;
    }

    public static final String OK = 'OK';

}