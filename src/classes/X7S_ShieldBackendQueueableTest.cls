/**
 * Created by jeffnelson on 10/30/18.
 */

@IsTest
private class X7S_ShieldBackendQueueableTest {

    @IsTest
    static void test_good() {

        Test.setMock(HttpCalloutMock.class, new MockResponse201());

        Test.startTest();
        X7S_ShieldBackendQueueable.ShieldStatus s = new X7S_ShieldBackendQueueable.ShieldStatus();
        s.applicant_id = '5719716599955456';
        s.new_status = X7S_ShieldBackendService.APPROVED;
        X7S_ShieldBackendQueueable.setApprovalStatus(
                new List<X7S_ShieldBackendQueueable.ShieldStatus> {s});
        Test.stopTest();
    }

    private class MockResponse201 implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {

            System.assertEquals('POST', req.getMethod());

            HttpResponse httpResponse = new HttpResponse();
            httpResponse.setStatusCode(201);
            return httpResponse;
        }
    }
}