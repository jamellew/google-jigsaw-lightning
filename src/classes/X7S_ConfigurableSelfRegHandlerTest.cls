/**
 * Created by jeffnelson on 12/11/18.
 */

@IsTest
private class X7S_ConfigurableSelfRegHandlerTest {

    @IsTest
    static void testBehavior() {
        Account a = new Account(Name = 'Test Account');
        insert a;

        Profile p = [Select ID from Profile where Name = 'Shield Authenticated User'];

        Map<SObjectField, String> registrationAttributes = new Map<SObjectField, String>{
                User.Email.getDescribe().getSObjectField() => 'test@test.com'};

        Test.startTest();
        new X7S_ConfigurableSelfRegHandler().createUser(
                a.Id
                ,p.Id
                ,registrationAttributes
                ,null);
        Test.stopTest();
    }

}