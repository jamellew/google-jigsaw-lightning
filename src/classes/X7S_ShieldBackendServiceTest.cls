/**
 * Created by jeffnelson on 10/30/18.
 */

@IsTest
private class X7S_ShieldBackendServiceTest {

    @IsTest
    static void test_good() {

        Test.setMock(HttpCalloutMock.class, new MockResponse201());

        Test.startTest();
        X7S_ShieldBackendService.updateUserApprovalStatus(
                new X7S_ShieldBackendService.ShieldStatus(
                        '123',X7S_ShieldBackendService.APPROVED));
        Test.stopTest();
    }

    @IsTest
    static void test_request_error() {

        Test.setMock(HttpCalloutMock.class, new MockResponse400());

        Test.startTest();
        try {
            X7S_ShieldBackendService.updateUserApprovalStatus(
                    new X7S_ShieldBackendService.ShieldStatus(
                            '123',X7S_ShieldBackendService.APPROVED));
        } catch (X7S_ShieldBackendService.ShieldException se) {
        }
        Test.stopTest();
    }

    @IsTest
    static void test_thrown_error() {

        Test.setMock(HttpCalloutMock.class, new MockResponseExcept());

        Test.startTest();
        try {
            X7S_ShieldBackendService.updateUserApprovalStatus(
                    new X7S_ShieldBackendService.ShieldStatus(
                            '123',X7S_ShieldBackendService.APPROVED));
        } catch (X7S_ShieldBackendService.ShieldException se) {
        }
        Test.stopTest();
    }

    private class MockResponse201 implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {

            System.assertEquals('POST', req.getMethod());

            HttpResponse httpResponse = new HttpResponse();
            httpResponse.setStatusCode(201);
            return httpResponse;
        }
    }

    private class MockResponse400 implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {

            System.assertEquals('POST', req.getMethod());

            HttpResponse httpResponse = new HttpResponse();
            httpResponse.setStatusCode(400);
            return httpResponse;
        }
    }

    private class MockResponseExcept implements HttpCalloutMock {

        public HTTPResponse respond(HTTPRequest req) {
            throw new X7S_ShieldBackendService.ShieldException('FAIL!');
        }
    }
}