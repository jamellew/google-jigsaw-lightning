/**
 * Created by jeffnelson on 11/27/18.
 */

@IsTest
private class ShieldAdminInvitationTriggerTest {

    @IsTest
    static void test_noContact() {
        Shield_Admin_Invitations__e s = new Shield_Admin_Invitations__e(
            Contact_Email__c = 'test@test.com'
            ,Invitation_Status__c = 'Approved - Pending'
            ,Organization_Name__c = 'Test'
            ,Organization_Website__c = 'test.com');

        Test.startTest();
        EventBus.publish(s);
        Test.stopTest();

        System.assertEquals(s.Contact_Email__c,[select Email from Contact limit 1].Email);
    }

    @IsTest
    static void test_existingContact() {
        Account a = new Account(
                Name = 'Test'
                ,Website = 'test.com');
        insert a;

        Contact c = new Contact(
                LastName = 'test@test.com'
                ,Email = 'test@test.com'
                ,AccountId = a.Id);
        insert c;

        Shield_Admin_Invitations__e s = new Shield_Admin_Invitations__e(
                Contact_Email__c = 'test@test.com'
                ,Invitation_Status__c = 'Approved - Pending'
                ,Organization_Name__c = 'Test'
                ,Organization_Website__c = 'test.com');

        Test.startTest();
        EventBus.publish(s);
        Test.stopTest();

        System.assertEquals(s.Invitation_Status__c,[select Invitation_Status__c from Contact limit 1].Invitation_Status__c);
    }

}