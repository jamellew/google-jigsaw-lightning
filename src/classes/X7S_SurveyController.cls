/**
 * Created by jeffnelson on 11/15/18.
 */

public with sharing class X7S_SurveyController {


    public static void setUniqueIds (List<Survey__c> surveys) {

        for(Survey__c s : surveys) {
            s.Unique_ID__c = EncodingUtil.convertToHex(
                    Crypto.generateDigest(
                            'SHA-256',
                            Blob.valueOf(s.Name + s.Case__c + s.Contact__c)));
        }

    }

    @AuraEnabled
    public static Survey__c loadSurvey (String uniqueId) {

        Survey__c ret = null;
        for(Survey__c s : [
                SELECT
                        Case__c
                        ,Contact__c
                        ,Is_your_issue_resolved__c
                        ,Other_Feedback__c
                        ,Support_Experience_Rating__c
                        ,Id
                FROM
                        Survey__c
                WHERE
                        Unique_ID__c = :uniqueId
                        AND Completed__c = false
                LIMIT
                        1]) {
            ret = s;
        }
        return ret;
    }

    @AuraEnabled
    public static void updateSurvey (Survey__c survey) {

        survey.Completed__c = true;
        update survey;

    }

}