/**
 * Created by jeffnelson on 10/29/18.
 */

public with sharing class X7S_ShieldBackendService {

    private static final String NAMED_CREDENTIAL = 'callout:Shield_Backend/applicants/status';

    public static final Integer APPLIED = 0;
    public static final Integer ONHOLD = 1;
    public static final Integer APPROVED = 2;
    public static final Integer REJECTED = 3;
    public static final Integer SCREENING = 4;

    public static void updateUserApprovalStatus (ShieldStatus status) {

        Http http = new Http();
        HttpRequest httpRequest = new HttpRequest();
        httpRequest.setEndpoint(NAMED_CREDENTIAL);
        httpRequest.setMethod('POST');
        httpRequest.setTimeout(100000);
        httpRequest.setHeader('Content-Type', 'application/json');
        httpRequest.setBody(
            JSON.serialize(status));

        System.debug(httpRequest.getBody());

        HttpResponse httpResponse;
        try {
            httpResponse = http.send(httpRequest);
        } catch (Exception e) {
            throw new ShieldException(e);
        }

        System.debug(httpResponse.getBody());

        if (httpResponse.getStatusCode() != 201) {
            throw new ShieldException(httpResponse.getBody());
        }
    }

    public class ShieldStatus {

        public Integer new_status;
        public List<Long> applicant_ids;

        public ShieldStatus (String appId, Integer status) {
            this.new_status = status;
            this.applicant_ids = new List<Long>{Long.valueOf(appid)};
        }
    }

    public class ShieldException extends Exception {}
}