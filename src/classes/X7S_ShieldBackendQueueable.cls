/**
 * Created by jeffnelson on 10/29/18.
 */

public with sharing class X7S_ShieldBackendQueueable implements Queueable, Database.AllowsCallouts {

    public ShieldStatus status;

    public X7S_ShieldBackendQueueable (ShieldStatus status) {
        this.status = status;
    }

    public void execute(QueueableContext context) {
        X7S_ShieldBackendService.updateUserApprovalStatus(
                new X7S_ShieldBackendService.ShieldStatus(status.applicant_id,status.new_status));
    }

    @InvocableMethod(label='Update Shield approval status.' description='Updates the status of the specified email in the Shield backend service.')
    public static void setApprovalStatus(List<ShieldStatus> statuses) {
        for(ShieldStatus status : statuses) {
            System.enqueueJob(
                    new X7S_ShieldBackendQueueable(status));
        }
    }

    public class ShieldStatus {

        @InvocableVariable(required=true)
        public Integer new_status;
        @InvocableVariable(required=true)
        public String applicant_id;

        public ShieldStatus () {}
    }

}