/**
 * Created by jeffnelson on 11/15/18.
 */

@IsTest
private class X7S_SurveyControllerTest {

    @testSetup
    static void setup() {
        insert new List<Survey__c>{
                new Survey__c()
        };
    }

    @IsTest
    static void test_setUniqueIds() {

        Test.startTest();
        for(Survey__c s : [SELECT Unique_ID__c from Survey__c]) {
            System.assertNotEquals(null,s.Unique_ID__c);
        }
        Test.stopTest();
    }

    @IsTest
    static void test_loadSurvey() {

        Survey__c s = [SELECT Unique_Id__c from Survey__c LIMIT 1];

        Test.startTest();
        System.assertEquals(
                s.Id
                ,X7S_SurveyController.loadSurvey(s.Unique_Id__c).Id);
        Test.stopTest();
    }

    @IsTest
    static void test_updateSurvey() {

        Survey__c s = new Survey__c();
        insert s;

        Test.startTest();
        s.Is_your_issue_resolved__c = 'Yes';
        s.Other_Feedback__c = 'Testy test test test.';
        X7S_SurveyController.updateSurvey(s);
        Test.stopTest();

        System.assertEquals(true,[select Completed__c from Survey__c where Id = :s.Id].Completed__c);
    }

}