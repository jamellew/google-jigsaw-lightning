/**
 * Created by jeffnelson on 11/27/18.
 */

@IsTest
private class ShieldUserStatusTriggerTest {

    @IsTest
    static void test_existingContact() {

        Contact c = new Contact(
                LastName = 'test@test.com'
                ,Email = 'test@test.com');
        insert c;

        Shield_User_Status__e s = new Shield_User_Status__e(
                Contact_Email__c = 'test@test.com'
                ,Contact_Status__c = 'Approved - Pending'
                ,First_Name__c = 'tfirst'
                ,Last_Name__c = 'flast'
        );

        Test.startTest();
        EventBus.publish(s);
        Test.stopTest();

        System.assertEquals(s.Contact_Status__c,[select Invitation_Status__c from Contact limit 1].Invitation_Status__c);

    }

}