<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>false</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Scheduled announcements to surface in Lightning Communities.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableChangeDataCapture>false</enableChangeDataCapture>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Announce_to_Community__c</fullName>
        <externalId>false</externalId>
        <label>Announce to Community</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>All</fullName>
                    <default>true</default>
                    <label>All</label>
                </value>
                <value>
                    <fullName>Peak Announcements</fullName>
                    <default>false</default>
                    <label>Peak Announcements</label>
                </value>
                <value>
                    <fullName>Test Community</fullName>
                    <default>false</default>
                    <label>Test Community</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Announcement_Body__c</fullName>
        <externalId>false</externalId>
        <label>Announcement Body</label>
        <length>500</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Announcement_Call_to_Action__c</fullName>
        <description>The call to action that a user will click on to be taken to a provided URL.</description>
        <externalId>false</externalId>
        <inlineHelpText>The call to action that a user will click on to be taken to a provided URL.</inlineHelpText>
        <label>Announcement Call to Action</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Announcement_Channel__c</fullName>
        <externalId>false</externalId>
        <label>Announcement Channel</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <valueSet>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>Default</fullName>
                    <default>true</default>
                    <label>Default</label>
                </value>
                <value>
                    <fullName>All</fullName>
                    <default>false</default>
                    <label>All</label>
                </value>
                <value>
                    <fullName>Peak Announcements: Product Page</fullName>
                    <default>false</default>
                    <label>Peak Announcements: Product Page</label>
                </value>
                <value>
                    <fullName>Test Community: Groups</fullName>
                    <default>false</default>
                    <label>Test Community: Groups</label>
                </value>
                <value>
                    <fullName>Custom Channel</fullName>
                    <default>false</default>
                    <label>Custom Channel</label>
                </value>
                <value>
                    <fullName>Page A</fullName>
                    <default>false</default>
                    <label>Page A</label>
                </value>
                <value>
                    <fullName>Page B</fullName>
                    <default>false</default>
                    <label>Page B</label>
                </value>
            </valueSetDefinition>
        </valueSet>
        <visibleLines>5</visibleLines>
    </fields>
    <fields>
        <fullName>Announcement_Dismissible__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Dismissible?</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Announcement_Type__c</fullName>
        <description>Choose the type of announcement.</description>
        <externalId>false</externalId>
        <inlineHelpText>Choose the type of announcement. Notification = general/passive, critical = severe.</inlineHelpText>
        <label>Announcement Type</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
        <valueSet>
            <restricted>true</restricted>
            <valueSetDefinition>
                <sorted>false</sorted>
                <value>
                    <fullName>info</fullName>
                    <default>true</default>
                    <label>Notification</label>
                </value>
                <value>
                    <fullName>warning</fullName>
                    <default>false</default>
                    <label>Alert</label>
                </value>
                <value>
                    <fullName>error</fullName>
                    <default>false</default>
                    <label>Critical</label>
                </value>
            </valueSetDefinition>
        </valueSet>
    </fields>
    <fields>
        <fullName>Announcement_URL__c</fullName>
        <description>The URL the user is taken to when clicking on the call to action text.</description>
        <externalId>false</externalId>
        <inlineHelpText>The URL the user is taken to when clicking on the call to action text.</inlineHelpText>
        <label>Announcement URL</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Url</type>
    </fields>
    <fields>
        <fullName>End_Date_Time__c</fullName>
        <description>Date and time to stop showing this announcement in the community.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date and time to stop showing this announcement in the community.</inlineHelpText>
        <label>End Date &amp; Time</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Order__c</fullName>
        <externalId>false</externalId>
        <formula>CASE(Announcement_Type__c, &quot;info&quot;, 3, &quot;warning&quot;, 2, &quot;error&quot;, 1, 3)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Order</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Send_Email_c__c</fullName>
        <defaultValue>false</defaultValue>
        <description>Check to send email to Community Users</description>
        <externalId>false</externalId>
        <label>Send_Email__c</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Start_Date_Time__c</fullName>
        <description>Date and time to start showing this announcement in the community.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date and time to start showing this announcement in the community.</inlineHelpText>
        <label>Start Date &amp; Time</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <label>Community Announcement</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>Announcement_Channel__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>All1</fullName>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Announcement Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Community Announcements</pluralLabel>
    <searchLayouts/>
    <sharingModel>ReadWrite</sharingModel>
    <visibility>Public</visibility>
</CustomObject>
