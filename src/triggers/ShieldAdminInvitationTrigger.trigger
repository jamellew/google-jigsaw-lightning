/**
 * Created by jeffnelson on 11/27/18.
 */

trigger ShieldAdminInvitationTrigger on Shield_Admin_Invitations__e (after insert) {

    Set<String> emails = new Set<String>();
    Set<String> accountNames = new Set<String>();
    Set<String> accountWebsites = new Set<String>();
    for(Shield_Admin_Invitations__e s : Trigger.new) {
        if(String.isNotEmpty(s.Contact_Email__c)) emails.add(s.Contact_Email__c);
        if(String.isNotEmpty(s.Organization_Name__c)) accountNames.add(s.Organization_Name__c);
        if(String.isNotEmpty(s.Organization_Website__c)) accountWebsites.add(s.Organization_Website__c);
    }

    Map<String,Contact> email2Contact = new Map<String,Contact>();
    for(Contact c : [select Email from Contact where Email in :emails]) {
        email2Contact.put(c.Email,c);
    }

    Map<String,Account> name2Account = new Map<String,Account>();
    for(Account a : [select Name,Website from Account where Name in :accountNames and Website in :accountWebsites]) {
        name2Account.put(a.Name,a);
    }

    List<Account> accountsToInsert = new List<Account>();
    for(Shield_Admin_Invitations__e s : Trigger.new) {
        if(!name2Account.containsKey(s.Organization_Name__c)) {
            Account a = new Account(
                    Name = s.Organization_Name__c
                    ,Website = s.Organization_Website__c);
            accountsToInsert.add(a);
            name2Account.put(a.Name,a);
        }
    }
    insert accountsToInsert;

    List<Contact> contactsToInsert = new List<Contact>();
    List<Contact> contactsToUpdate = new List<Contact>();
    for(Shield_Admin_Invitations__e s : Trigger.new) {
        if(email2Contact.containsKey(s.Contact_Email__c)) {
            // We found a contact, update it.
            Contact c = email2Contact.get(s.Contact_Email__c);
            c.Invitation_Status__c = s.Invitation_Status__c;
            c.Link_to_Shield_Invitation__c = s.URL__c;
            c.AccountId = name2Account.get(s.Organization_Name__c).Id;
            contactsToUpdate.add(c);
        } else {
            // Create a new contact
            Contact c = new Contact(
                    LastName = s.Contact_Email__c
                    ,Email = s.Contact_Email__c
                    ,AccountId = name2Account.get(s.Organization_Name__c).Id
                    ,Invitation_Status__c = s.Invitation_Status__c
                    ,Link_to_Shield_Invitation__c = s.URL__c);
            contactsToInsert.add(c);
        }
    }

    insert contactsToInsert;
    update contactsToUpdate;

}