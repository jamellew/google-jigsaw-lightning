/**
 * Created by jeffnelson on 11/15/18.
 */

trigger SurveyTrigger on Survey__c (before insert) {

    X7S_SurveyController.setUniqueIds(Trigger.new);

}