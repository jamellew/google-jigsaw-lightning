/**
 * Created by jeffnelson on 11/27/18.
 */

trigger ShieldUserStatusTrigger on Shield_User_Status__e (after insert) {

    Set<String> emails = new Set<String>();

    for(Shield_User_Status__e s : Trigger.new) {
        if(String.isNotEmpty(s.Contact_Email__c)) emails.add(s.Contact_Email__c);
    }

    Map<String,Contact> email2Contact = new Map<String,Contact>();
    for(Contact c : [select Email from Contact where Email in :emails]) {
        email2Contact.put(c.Email,c);
    }

    List<Contact> contactsToUpdate = new List<Contact>();
    for(Shield_User_Status__e s : Trigger.new) {
        if (email2Contact.containsKey(s.Contact_Email__c)) {
            // We found a contact, update it.
            Contact c = email2Contact.get(s.Contact_Email__c);
            c.Invitation_Status__c = s.Contact_Status__c;
            c.Shield_Status__c = s.Contact_Status__c;
            if(String.isNotEmpty(s.First_Name__c)) c.FirstName = s.First_Name__c;
            if(String.isNotEmpty(s.Last_Name__c)) c.LastName = s.Last_Name__c;
            contactsToUpdate.add(c);
        }
    }

    update contactsToUpdate;

}