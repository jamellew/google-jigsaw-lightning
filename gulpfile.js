var cache       = require('gulp-cached');
var concat      = require('gulp-concat');
var eslint      = require('gulp-eslint');
var prettier    = require('gulp-prettier');
var gulp        = require('gulp');
var header      = require('gulp-header');
var less        = require('gulp-less');
var notify      = require('gulp-notify');
var pkg         = require('./package.json');
var plumber     = require('gulp-plumber');

// -------------------------------------------------------------------------------------------------
// Shared resource path locations
// -------------------------------------------------------------------------------------------------
// jsWatchMin - Files that will be watched then minified into a single Javascript file upon changes
// jsLint - Files that will be ran through the linter
// cssBootstrap - File that will be used to compile LESS to CSS
// cssWatchMin - Files that will be watched for changes then compiled upon changes
// -------------------------------------------------------------------------------------------------
var srcPaths = {
    jsWatchMin: ['scripts/**/*.js', '!scripts/vendor/**/*.js'],
    jsLint: ['scripts/**/*.js', '!scripts/vendor/**/*.js'],
    overrides: ['styles/overrides/overrides.less'],
    components: [
        'styles/components/**/*.less',
        '!styles/components/common.less',
        '!styles/components/_base/**/*.less',
        '!styles/components/_salesforce/**/*.less'
    ],
    cleanCSS: ['./styles/**/*.less'],
    cssWatchMin: ['styles/**/*.less']
};

// -------------------------------------------------------------------------------------------------
// Build resource path locations
// -------------------------------------------------------------------------------------------------
// js - Compiled/minified Javascript output location
// css - Compiled/minified CSS output location
// -------------------------------------------------------------------------------------------------
var buildPaths = {
    js: 'src/staticresources/' + pkg.namespace + '/scripts',
    overrides: 'src/staticresources/' + pkg.namespace + '/styles',
    fonts: 'src/staticresources/' + pkg.namespace + '/styles/fonts',
    components: 'src/aura/'
};

// -------------------------------------------------------------------------------------------------
// Error handler
// -------------------------------------------------------------------------------------------------
var onError = function (err) {
    notify.onError({
        title: 'Gulp',
        subtitle: 'Failure!',
        message: 'Error: <%= error.message %>'
    })(err);
    this.emit('end');
};

// -------------------------------------------------------------------------------------------------
// Header appended to all compiled files as a notice
// -------------------------------------------------------------------------------------------------
var compiledBanner = [
    '/**',
    ' * <%= pkg.name %> - <%= pkg.description %>',
    ' * @version <%= pkg.version %>',
    ' * @author <%= pkg.author %>',
    ' *',
    ' * COMPILED FILE DO NOT DIRECTLY EDIT',
    ' */',
    ''
].join('\n');

// -------------------------------------------------------------------------------------------------
// CSS Tasks
// -------------------------------------------------------------------------------------------------

// Runs postCSS parser on all custom LESS files.
// https://prettier.io
// -----------------------------
gulp.task('cleanCSS', function () {
    return gulp.src(srcPaths.cleanCSS, {base: './'})
        .pipe(cache('cleanCSS'))
        .pipe(plumber({errorHandler: onError}))
        .pipe(prettier())
        .pipe(gulp.dest('./'));
});

// Compile LESS files into CSS
// -----------------------------
gulp.task('compileComponentCSS', gulp.series('cleanCSS', function () {
    return gulp
        .src(srcPaths.components)
        .pipe(cache('componentCSS'))
        .pipe(plumber({errorHandler: onError}))
        .pipe(less())
        .pipe(header(compiledBanner, {pkg: pkg}))
        .pipe(gulp.dest(buildPaths.components))
        .pipe(
            notify({
                title: 'Gulp',
                subtitle: 'Success',
                message: 'LESS compiled: <%= file.relative %>'
            })
        );
    })
);

gulp.task('compileOverrideCSS', gulp.series('cleanCSS', function () {
    return gulp
        .src(srcPaths.overrides)
        .pipe(cache('overrideCSS'))
        .pipe(plumber({errorHandler: onError}))
        .pipe(less())
        .pipe(header(compiledBanner, {pkg: pkg}))
        .pipe(gulp.dest(buildPaths.overrides))
        .pipe(
            notify({
                title: 'Gulp',
                subtitle: 'Success',
                message: 'LESS compiled: <%= file.relative %>'
            })
        );
    })
);

// Build CSS tasks
// -----------------------------
gulp.task('buildCSS', gulp.parallel('compileComponentCSS', 'compileOverrideCSS'));


// -------------------------------------------------------------------------------------------------
// Javascript Tasks
// -------------------------------------------------------------------------------------------------

// Runs JS parser on all custom JS files.
// https://prettier.io
// -----------------------------
gulp.task('cleanJS', function () {
    return gulp.src(srcPaths.jsLint, {base: './'})
        .pipe(cache('cleanJS'))
        .pipe(plumber({errorHandler: onError}))
        .pipe(prettier())
        .pipe(gulp.dest('./'));
});

// Runs JSLint and formats all custom JavaScript files except 'vendor' files in the src/js/vendor directory.
//
// Configuration file: .eslintrc
// uncomment last line to halt on errors
// -----------------------------
gulp.task('lint', function () {
    return gulp.src(srcPaths.jsLint, {base: './'})
        .pipe(eslint())
        .pipe(eslint.format())
        // .pipe(eslint.failAfterError())
});

// Lint, prettify and concatenate all the JavaScript in the jsWatchMin paths
// -----------------------------
gulp.task('concatJS', gulp.series('lint', 'cleanJS', function () {
    return gulp.src(srcPaths.jsWatchMin)
        .pipe(cache('concatJS'))
        .pipe(plumber({errorHandler: onError}))
        .pipe(concat(pkg.namespace + '.js'))
        .pipe(header(compiledBanner, { pkg : pkg } ))
        .pipe(gulp.dest(buildPaths.js))
        .pipe(
            notify({
                title: 'Gulp',
                subtitle: 'Success',
                message: 'Concatenated: <%= file.relative %>'
            })
        );
    })
);

// Build JavaScript tasks
// -----------------------------
gulp.task('buildJS', gulp.parallel('concatJS'));


// -------------------------------------------------------------------------------------------------
// Watch Tasks
// -------------------------------------------------------------------------------------------------
gulp.task('watch:styles', function () {
    gulp.watch(srcPaths.components, gulp.series('compileComponentCSS', 'compileOverrideCSS'));
});

gulp.task('watch:js', function () {
    gulp.watch(srcPaths.jsWatchMin, gulp.series('concatJS'));
});

gulp.task('watch', gulp.parallel('watch:styles', 'watch:js'));

// -------------------------------------------------------------------------------------------------
// The default task (called when you run `gulp` from cli)
// -------------------------------------------------------------------------------------------------
gulp.task('default', gulp.series('buildCSS', 'buildJS', 'watch'));
